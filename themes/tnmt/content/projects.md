+++
title = "projects."
date = 2022-03-30
[extra]
active_key = "projects"
+++



Sed eleifend rutrum volutpat. Nam aliquet velit sed tellus condimentum, quis tincidunt nisi bibendum. Donec porta tellus eu magna egestas, vel posuere erat ultrices. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec ullamcorper elit. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In porta eget libero vel pellentesque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;

```python
#!/usr/bin/env python3

import sys


def PascalsTriangle(row):
    max_row = int(row)
    triangle = []
    current_row = 0

    while current_row < max_row:
        # These first two cases are short circuits that allow us to ignore
        # the invisible 0s that pad the 1s and can't ignore in these 2 rows
        if current_row == 0:
            members = [1]
        elif current_row == 1:
            members = [1, 1]
        # proceed as normal now that we are passed the first 2 rows
        else:
            pr_index = current_row - 1  # previous row's index
            members = [1]
            members += [triangle[pr_index][x] + triangle[pr_index][x+1]
                        for x in range(pr_index)]
            members += [1]
        triangle.append(members)
        current_row += 1

    return triangle


if __name__ == "__main__":
    try:
        count = sys.argv[1]
        tri = PascalsTriangle(count)
    except Exception as err:
        sys.stderr.write(("\nUsage: pt.py <int>\n"
                          "Example: pt.py 3\n"
                          "\t[[1]\n"
                          "\t[1, 1]\n"
                          "\t[1, 2, 1]]\n"))

    print("\n".join([str(row) for row in tri]))  # print it pretty, print it niiiice
```

Aliquam vitae ornare mauris, quis tristique nunc. Vestibulum quis turpis erat. Maecenas egestas a nisl eget feugiat. Donec mollis gravida ex, in feugiat massa congue at. Morbi dignissim tincidunt rhoncus. Nulla tristique faucibus velit, eget convallis massa imperdiet in. Etiam pretium ante nec turpis feugiat, vitae imperdiet risus mattis. Donec quis lorem ac ligula aliquam auctor. Cras eget dapibus lacus. In vel ipsum tellus. Nam iaculis mollis neque, sit amet mattis est fermentum et. Fusce et ligula sed est condimentum efficitur nec sed lorem. Curabitur sed nibh et magna laoreet mattis. Fusce nec mattis mauris. Mauris sit amet neque cursus, hendrerit massa id, rutrum augue.

Suspendisse eros nulla, fermentum at accumsan quis, vulputate a nunc. Donec in maximus neque, vestibulum auctor sapien. Ut scelerisque gravida suscipit. Nullam dictum quam enim, non rutrum augue laoreet sit amet. In purus erat, elementum et aliquet et, scelerisque id leo. Nunc eros est, fermentum in vestibulum quis, mollis sed nunc. Cras consectetur velit odio, a blandit tortor porttitor nec. In mattis efficitur ipsum non vestibulum. Pellentesque congue gravida molestie. Nam efficitur, augue in vehicula placerat, est leo vulputate ante, eu aliquet ipsum eros id quam. In sed sollicitudin justo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;
