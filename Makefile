ifneq (,)
.error This Makefile requires GNU Make.
endif

.ONESHELL:
SHELL := /bin/bash
.SHELLFLAGS := -eu -o pipefail -c
MAKEFLAGS += --warn-undefined-variables

PORT ?= 8888
OUTPUT_DIR ?= public
ZOLA ?= ./bin/zola
ZOLA_CONFIG ?= ./config.toml
ZOLA_VERSION := v0.15.3
TAILWINDCSS_VERSION := v3.3.2

build: download-deps build-theme public
.PHONY: build

build-theme: ## build theme
	make PATH="$$PWD/bin:$$PATH" -C ./themes/tnmt build
.PHONY: build-theme

watch: ## start dev server and reload automatically on content changes
	$(ZOLA) serve --drafts --port $(PORT)
.PHONY: watch

watch-nd: ## start dev server (no drafts) and reload automatically on content changes
	$(ZOLA) serve --port $(PORT)
.PHONY: watch-nd

public: ## regenerate the output directory
	$(ZOLA) --config $(ZOLA_CONFIG) build --output-dir $(OUTPUT_DIR)
.PHONY: build

download-deps: zola tailwindcss ## download tools to build theme and site
.PHONY: download-deps

clean: ## delete $OUTPUT_DIR contents
	rm -fr $(OUTPUT_DIR)
.PHONY: clean

clean-deps: ## delete build deps
	rm -fr _build bin/*
.PHONY: clean-deps

clean-all: clean clean-deps ## delete content output and deps
.PHONY: clean-all

_build:
	mkdir -p ./_build

## zola start
zola: bin/zola
.PHONY: zola

bin/zola: bin _build/zola.tar.gz ## download zola binary
	@tar -xzf ./_build/zola.tar.gz -C ./bin

_build/zola.tar.gz: _build
	@curl -sL "https://github.com/getzola/zola/releases/download/$(ZOLA_VERSION)/zola-$(ZOLA_VERSION)-x86_64-unknown-linux-gnu.tar.gz" > ./_build/zola.tar.gz
## zola final

## tailwindcss start
tailwindcss: bin/tailwindcss
PHONY: tailwindcss

bin/tailwindcss: ## download tailwindcss cli
	@curl -sL "https://github.com/tailwindlabs/tailwindcss/releases/download/$(TAILWINDCSS_VERSION)/tailwindcss-linux-x64" > ./bin/tailwindcss \
		&& chmod +x ./bin/tailwindcss
## tailwindcss final

help: ## show this help
	@echo "\nSpecify a command from the list below:\n"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "  \033[0;36m%-12s\033[m %s\n", $$1, $$2}'
	@echo ""
	@echo $(MAKEFILE_LIST)
.PHONY: help
