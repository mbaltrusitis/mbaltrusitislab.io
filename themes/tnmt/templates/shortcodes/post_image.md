{% set resized_image = resize_image(path=page.path ~ img_name, width=840, op="fit_width")  %}
<img src="{{ resized_image.url }}" alt="{{ alt | default(value="") }}" title="{{ title | default(value="") }}"/>
