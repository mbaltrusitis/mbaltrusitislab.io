const colors = require('tailwindcss/colors')

module.exports = {
  content: ["./templates/**/*.html"],
  theme: {
    extend: {
			colors: {
				violet: {
					950: '#2f253d',
				},
				backgroundColor: "#2c2136",
				textColor: "#f6f5f4",
				primaryColor: {
					100: "#8ff0a4",
					200: "#57e389",
					300: "#33d17a",
					400: "#2ec27e",
					500: "#26a269"
				},
				secondaryColor: {
					100: "#ffbe6f",
					200: "#ffa348",
					300: "#ff7800",
					400: "#e66100",
					500: "#c64600"
				}
			}
		},
		listStyleType: {
			decimal: 'decimal-leading-zero',
			disc: 'disc',
		},
  },
	safelist: [
		"float-right",
		"float-left",
	],
  variants: {},
  plugins: []
};
