{% if filename %}
	{% set snippet_url = url ~ "/raw/main/" ~ filename %}
{% else %}
	{% set snippet_url = url ~ "/raw" %}
{% endif %}
{% set source = load_data(url=snippet_url) %}
{# TODO: This could be made neater the linked issue is resolved
   allowing for nested shortcode calls.
   https://github.com/getzola/zola/issues/515#issuecomment-1583279097 #}

```{{ file_type }}
{{ source }}
```