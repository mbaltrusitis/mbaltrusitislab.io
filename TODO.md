# TODOs

- [] Consider moving the `tnmt` theme to its own repo
- [] Add search functionality provided by the search indexes generated by Zola
- [] Fix the bouncing footer that occurs when `hover:list-disc` is activated
- [] Add your `Projects` section
