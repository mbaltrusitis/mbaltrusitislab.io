+++
title="news"
date = 2022-03-30
[extra]
active_key = "news"
+++

## news item 1

 Etiam bibendum ullamcorper nulla, sit amet sagittis nunc porta eu. Aliquam erat volutpat. In lectus justo, interdum ac pretium et, convallis a lectus. Integer vulputate vitae purus vitae consequat. Curabitur tempus semper diam, et venenatis lectus sagittis id. Proin tristique viverra sem in dignissim. Mauris imperdiet quam sit amet pretium tristique. Nunc a aliquet lectus, varius eleifend nisi. Ut a faucibus orci. In vitae nibh eget purus blandit sollicitudin at a dolor. Nam faucibus turpis vel nisl auctor sollicitudin.

## news item 2

 Cras sit amet pellentesque lacus. Maecenas luctus lectus in lacus tristique, quis sollicitudin justo interdum. Vestibulum consectetur pulvinar lacus eu scelerisque. Proin imperdiet blandit dignissim. Vestibulum ut tincidunt dui, at eleifend turpis. Proin feugiat iaculis semper. Nullam diam lorem, convallis ut blandit ut, venenatis luctus nulla. Donec nec varius purus, in sagittis lacus. Aenean non diam in eros fermentum lobortis id sed neque. Vivamus quam ex, ultricies vel eleifend sed, posuere a nibh. Suspendisse facilisis bibendum justo, non tempus justo auctor ac. In iaculis pellentesque ultrices. Curabitur rhoncus, dui eu maximus tristique, urna ex tempus nisi, sed hendrerit dui est ut massa.

## news item 3

 Integer nunc urna, aliquam vel efficitur ac, lobortis quis velit. Vivamus in lacinia ipsum, eget cursus ante. Quisque vel ante diam. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum commodo dignissim massa, et venenatis velit tincidunt fringilla. Pellentesque tempus arcu non vulputate scelerisque. Nulla blandit enim quis neque lacinia, ut feugiat urna molestie. Proin accumsan posuere purus, et vulputate ante placerat sollicitudin. Nullam dictum sodales dui et dignissim.

## news item 4

 Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum porta pellentesque eros, ac consectetur metus lacinia sit amet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam posuere malesuada tortor, ut pulvinar dolor finibus id. Fusce sed sollicitudin orci, id aliquet tellus. Cras tincidunt iaculis dolor, sit amet volutpat sem. Donec consectetur pellentesque auctor. Nulla ornare metus ac fringilla convallis. Aliquam vel lectus nec leo consectetur laoreet at eu tellus. Maecenas nulla sapien, dictum et vehicula vitae, iaculis sit amet lectus. Integer sed dui nunc. Curabitur nec nibh congue, aliquet risus id, molestie diam.

## news item 5

 Aenean eget neque a nulla commodo egestas. Pellentesque accumsan metus in nulla maximus finibus. Donec eget magna sed quam tempor gravida. Duis sodales maximus accumsan. Quisque eget arcu pretium, dignissim justo at, feugiat lectus. Maecenas sed volutpat sem. Curabitur convallis maximus lectus, ut convallis urna viverra ac. Fusce viverra dignissim lorem in faucibus. Vivamus diam nunc, dapibus placerat ipsum ut, congue egestas nunc. Maecenas aliquam felis sed risus elementum suscipit. Vivamus nisl mi, porttitor non leo sit amet, tincidunt convallis turpis. Cras quam massa, sollicitudin eu iaculis non, fringilla at quam. Vestibulum eget lacus tincidunt, venenatis nulla eget, consectetur tellus. Donec sit amet eros in justo consequat tincidunt a eu dui. Ut id quam nisi. Sed risus arcu, egestas nec lectus eget, placerat rhoncus augue.

