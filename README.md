# notmatthew.io static site generator

## overview.

There are three main components that work together to generate the notmatthew.io site:

1. `tnmt` or, The Not Matthew Theme
2. The `notmatthew` repo, vendored in this repo as a git submodule
3. This repo! Who's main purpose is to pull together dependencies and run them through the build pipeline

## dependencies.

- Zola
- Tailwindcss CLI

## Quickstart

When starting from zero you must:

1. Pull the repo and init the content submodule
1. Install the dependencies listed above
1. Build the styles for `tnmt` by running its `style-build` Makefile goal
1. Generate the full site running `zola build`

## Development

1. Follow the instructions for Quickstart but instead of running the build versions of the last two commands, run the hotreload version:
	- make watch
	- make -C themes/tnmt style-build
